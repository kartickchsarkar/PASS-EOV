/*!
 * This function calculates/returns the soft (0.5-2.0keV) and hard (2.0-10.0keV)
 * x-ray emission function for any given temp and metallicty by interpolating 
 * the MEKAL data saved at emissivity/Z_...dat.
 *
 * t [I]	= Temperature
 * z [I]	= Metallicity in units of Solar metallicity
 * \return	= an array of lambda for soft (lambda[0]) and hard (lambda[1]) 
 */

#include "head.h"

void XrayLambda(double t, double z, double *lambda)
{
 int klo, kmid, khi;
 double tmid, lambdat1, lambdat2;
 double z1, z2, t1, t2, dz, dt;
 double dummyT, dummyLs, dummyLh;
 static int countT1, countT2;
 static double *T, **lambda1, **lambda2;
 FILE *fspec1, *fspec2;


 /* reading the datafiles containing T, Lx_soft, Lx_hard */
 if (T == NULL){
//     print1("\n> reading emissivity table for Z = 0.2 ...\n", 0);
     fspec1 = fopen("emissivity/Z_02.dat","r");
     if (fspec1 == NULL){ printf("! emissivity/Z_02.dat does not exist.\n"); exit(1);}
     T = calloc(1000, sizeof(double));
     lambda1 = ARRAY_2D(1000, 2, double);
     countT1 = 0;
     while (fscanf(fspec1,"%lf %lf %lf\n", &dummyT, &dummyLs, &dummyLh)!= EOF){
         T[countT1] 		= dummyT;
	 lambda1[countT1][0]	= dummyLs; 
	 lambda1[countT1][1]	= dummyLh;
	 countT1		+= 1;
     }

//     print1("> reading emissivity table for Z = 1.0 ...\n", 0);
     fspec2 = fopen("emissivity/Z_10.dat","r");
     if (fspec2 == NULL){ printf("! emissivity/Z_10.dat does not exist.\n"); exit(1);}
     lambda2 = ARRAY_2D(1000, 2, double);
     countT2 = 0;
     while (fscanf(fspec2,"%lf %lf %lf\n", &dummyT, &dummyLs, &dummyLh)!= EOF){
	 lambda2[countT2][0]	= dummyLs; 
	 lambda2[countT2][1]	= dummyLh;
	 countT2		+= 1;
     }
     fclose(fspec1); fclose(fspec2);
 }

 if (countT2 != countT1){ 	// checking if both the T tables have same length 
     printf ( "! Error while reading Z_...dat files. T length mismatch. \n Returning Now.\n"); 
     exit(1);
 }

 /* Data file reading ends */ 
 if (t < 1.e6){
     lambda[0] = 1.e-60;
     lambda[1] = 1.e-60;
     return;
 }
 if (t >= 2e8){
     lambda[0] = Lambda_freefree(t,0.5,2.0);
     lambda[1] = Lambda_freefree(t,2.0,10.0);
     return;
 }

 /* ** Interpolating emissivity ** */
 z1 = 0.2; 
 z2 = 1.0; 
 dz = z2-z1;
 
 /* finding location of Temperature in the table */
 klo = 0;
 khi = countT1 - 1;
 while (klo != (khi - 1)){
     kmid = (klo + khi)/2;
     tmid = T[kmid];
     if (t <= tmid){
         khi = kmid;
     }else if (t > tmid){
         klo = kmid;
     }
 }
 /* location of T found. */

 /* calculating lambda */
 t1 = T[klo];
 t2 = T[khi];
 dt = t2 - t1;
 lambdat1 = lambda2[klo][0]*(z-z1)/dz + lambda1[klo][0]*(z2-z)/dz;
 lambdat2 = lambda2[khi][0]*(z-z1)/dz + lambda1[khi][0]*(z2-z)/dz;
 lambda[0] = lambdat2*(t-t1)/dt + lambdat1*(t2-t)/dt;			// soft
 lambdat1 = lambda2[klo][1]*(z-z1)/dz + lambda1[klo][1]*(z2-z)/dz;
 lambdat2 = lambda2[khi][1]*(z-z1)/dz + lambda1[khi][1]*(z2-z)/dz;
 lambda[1] = lambdat2*(t-t1)/dt + lambdat1*(t2-t)/dt;			// hard

 return;

}



/**************************************************** */
double Lambda_freefree (double temp, double Emin, double Emax)
/*! 
 * Calculates free-free x-ray emission at the given location.
 * rho [I]      = Density in  mp/cc
 * temp [I]     = Temperature in K
 * E_min [I]    = Minimum energy of integration
 * E_max [I]    = Maximum energy of integration
 * \return      = Bremsstrahlung emissivity (erg/s/cc)
 * ************************************************** */
{
 double frac;

 frac = exp(-1.16*Emin/temp*1.e7) - exp(-1.16*Emax/temp*1.e7);
 return 1.4e-27*sqrt(temp)*1.2*frac;

}
