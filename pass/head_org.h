

#ifndef HEAD_H
#define HEAD_H

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

/* Define macros */
#define VERSION 1.2

#define YES     1
#define NO      0

#define PARALLEL	YES

#if PARALLEL
  #include<mpi.h>
#endif

#define CONST_G 6.67e-8
#define kB      1.38e-16
#define CONST_PI 3.14159265359
#define pc      3.08e18
#define kpc     (1.e3*pc)
#define Msun    2.e33
#define mp	 1.67e-24
#define km      1.e5
#define yr      3.15e7
#define Myr     (1.e6*yr)

#define DIMENSIONS	3
#define COMPONENTS	3

#if DIMENSIONS == 1
    #define EXPAND(a,b,c)       a
#elif DIMENSIONS == 2
    #define EXPAND(a,b,c)       a b
#elif DIMENSIONS == 3 || COMPONENTS == 3
    #define EXPAND(a,b,c)       a b c
#endif

/* ***************************************************************************
 * Galactic model definitions
 * => required only in case extended box is considered otherwise do not modify
 * *************************************************************************** */

#include "grav.h"

/* ***************************************************
 * Simulation details
 ***************************************************** */
#define CARTESIAN	YES
#define SPHERICAL	NO
#define CYLINDRICAL	NO
#define CONSIDER_EXT_BOX	YES

#if CARTESIAN
  #define xbox	1.0
  #define ybox	1.0
  #define zbox	1.0
#elif SPHERICAL
  #define rbox	1.0
#elif CYLINDRICAL
  #define Rbox	1.0
  #define zbox	1.0
#endif

#define EXISTS_NEG_Y	YES
#define EXISTS_NEG_Z	YES

#if CONSIDER_EXT_BOX
  #define rout	100.0
#endif

#define nx	256
#define ny	256
#define nz	256
#define nvar	5
#define dtype	float

#define idx_rho	0
#define idx_vx1	1
#define idx_vx2	2
#define idx_vx3	3
#define idx_prs	4
#define idx_met	0
#define unit_l	1.0		//in kpc
#define unit_rho	1.0		//in m_p/cc
#define unit_v	100.0		// km/s

#define single_file	NO

/* **********************************************
 * output emission type specifications
 ************************************************ */
#define FREEFREE	NO
#define MEKAL	NO
#define MEKAL_NOSPECTRA	NO
#define APEC	YES
#define RS	NO
#define COLDEN	NO
#define EM	NO
#define OTH	NO



/* **********************************************
 * Projected surface specification
 ************************************************ */
#define g_mtotal	32
#define g_ntotal	32
#define ds	0.1
#define THETA	1.57

#if CARTESIAN
  #if EXISTS_NEG_Y
    #define dy0	 	 (2.0*ybox/(g_mtotal*1.0))	// from -ybox to +ybox
  #else
    #define dy0                 (ybox/(g_mtotal*1.0))
  #endif
  #if EXISTS_NEG_Z
      #define dz0               (2.0*zbox/(g_ntotal*1.0))
  #else
      #define dz0               (zbox/(g_ntotal*1.0))
  #endif

#elif SPHERICAL
  #if EXISTS_NEG_Y
    #define dy0                 (2.0*rbox/(g_mtotal*1.0))
  #else
    #define dy0                 (rbox/(g_mtotal*1.0))
  #endif
  #if EXISTS_NEG_Z
    #define dz0                 (2.0*rbox/(g_ntotal*1.0))
  #else
    #define dz0                 (rbox/(g_ntotal*1.0))
  #endif

#elif CYLINDRICAL
  #if EXISTS_NEG_Y
    #define dy0                 (2.0*Rbox/(g_mtotal*1.0))
  #else
    #define dy0                 (Rbox/(g_mtotal*1.0))
  #endif
  #if EXISTS_NEG_Z
      #define dz0               (2.0*zbox/(g_ntotal*1.0))
  #else
      #define dz0               (zbox/(g_ntotal*1.0))
  #endif
#endif

/* *****************************************************
 * Output filename specifications
 * ***************************************************** */
#define g_logFile	YES
#define WRITESPECTRA	YES
#define AUTOMATIC_FILENAME	YES

static char *SpectraModelFolder	= "/home/sarkar/softwares/pimms/model_alt/";
static char *GridFile           = "grid1.out";
static char *g_outfile          = "output/user.txt";



#include "globals.h"



#endif

