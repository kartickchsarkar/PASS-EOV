

#include "head.h"

void MergeSBs();

int main(int argc, char *argv[])
/*  
 *Calculates the projected data of our Galaxy if looked from the solar system.
 * The calculations are done in sky coordinates (l,b).
 * ------------------------------------------------------------------
 * This function can calculate projected
 * 1) Mekal X-ray brightness in erg/s/cm^2/sr (option is MEKAL == 1)
 * 2) Free-free X-ray brightness in erg/s/cm^2/sr (option is FREEFREE == 1)
 * 2) Emission measure in cm^{-2} in 2.4-3.6 keV band (EM ==1)
 * 3) Hadronic gamma-ray brighness in erg/s/cm^2/sr (GAMMA_HAD == 1)
 * 4) Leptonic gamma-ray brighness in erg/s/cm^2/sr (GAMMA_LEP == 1)
 * 5) 23 GHz radio brightness in erg/s/cm^2/sr/Hz (SYNC23G == 1)
 * 5) HI column density in cm^{-2} from a vantage point parallel to the stellar disc (COLDEN == 1)
 * -----------------------------------------------------------------------
 * Last modified: 08.01.2016 by Kartick C Sarkar, email: kcsarkar@rri.res.in
*/

{
 start_time = time(NULL);

 int i, j, m, n;
 double x0, y0, z0, r0, th0, r, theta, modr;
 char filename[50], str2[100];
 FILE *fout;
 Data data; 

 FILE *gridfile = fopen (GridFile,"r");
 g_usedMem = 0;

 /* Initiate parallelisation */
 int mbeg, mend, dm;
 int status;
 int prank, nprocs;
 
 NARG = argc;
 
 #if PARALLEL
 MPI_Init(&argc, &argv);	// Initialise MPI and find number and set ranks of processors
 MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
 MPI_Comm_rank(MPI_COMM_WORLD, &prank);
 #else
 nprocs	= 1; 
 prank	= 0;
 #endif

 FILE *fp[nprocs];
 dm	= g_mtotal/nprocs;
 mbeg	= -g_mtotal/2*EXISTS_NEG_Y + dm*prank;		// dividing the job limits to processors
 mend	= mbeg + dm;

 sprintf(filename, "output/.outfile_%d.tmp",prank);	// Creating ouput filenames for processors
 status = remove(filename);				// remove pre-existing temporary files
 fp[prank] = fopen(filename, "a");
 /* Initialisation done*/


 WriteBegingingOfCode(prank);
 fout = ChooseOutputName(argv[1], prank);		// choosing final output filename

 /* Writting the header of the output file */
 fprintf(fout, "# from file %s\n", argv[1]);

 AllocateMemories(prank, &data); 
 StoreGridFile(gridfile, prank);

 StoreData(argv, prank, &data);		// Storing the simulation data

 #if MEKAL || APEC || RS
   SaveMekalModels(prank);		 /* storing the mekal model */
 #endif

 char msg2[200];
 sprintf(msg2,"> PASS has been parallelised across %d processors.", nprocs);
 print1(msg2, prank); 
 print1("> .................................\n> Starting Computation ...\n", prank); 

 /***** calculating the variable wanted; looping over Y and Z axis
  * ************************************************************** */

 for (m=mbeg; m<mend; m++){ 		// y loop for one proc 
     #if EXISTS_NEG_Z
     for (n=-g_ntotal/2; n<g_ntotal/2; n++){ // z loop
     #else
     for (n=0; n<g_ntotal; n++){
     #endif
         IntegrateAlongLOS(m, n, fp[prank], prank, &data);
     }
     fprintf (fp[prank],"\n");
     sprintf(str2, "> Task completed = %3.3f %%.", (m-mbeg)*100.0/(mend-mbeg) );
     print1( str2, prank);
 }

 fclose(fp[prank]); 
 FreeMemories(prank, &data);

 #if PARALLEL
 MPI_Barrier(MPI_COMM_WORLD);
 MergeSBs(); 		// Merging surface brightness data from different processors
 #else
 SB = SBproc;
 #endif

 FreeArray3D(g_mtotal, g_ntotal, (void ***)SBproc);
 if (prank == 0){
     CombineOutput(fout, nprocs); 
     AnalyseData(argv[1]);
 }

 #if PARALLEL
 MPI_Finalize();
 #endif

 return 0;
}

/* ************************************** */
#if PARALLEL
void MergeSBs()
{
 int i, j, k, m, n;
 int j1, k1, kbeg, jbeg, kend, jend, koffset, joffset;
 
 #if EXISTS_NEG_Y
   kbeg = -g_mtotal/2; kend = g_mtotal/2; koffset = g_mtotal/2;
 #else
   kbeg = 0; kend = g_mtotal; koffset = 0;
 #endif

 #if EXISTS_NEG_Z
   jbeg = -g_ntotal/2; jend = g_ntotal/2; joffset = g_ntotal/2;
 #else
   jbeg = 0; jend = g_ntotal; joffset = g_ntotal/2;
 #endif

 for (k=kbeg; k<kend; k++)
 for (j=jbeg; j<jend; j++){
     k1 = koffset + k;
     j1 = joffset + j ;
     MPI_Reduce(SBproc[k1][j1], SB[k1][j1], 2, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
 }

}
#endif
