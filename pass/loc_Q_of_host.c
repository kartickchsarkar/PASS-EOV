
#include "head.h"

void FindLocalQOfHost (int *ii, double *Q, Data *data)
/* ***********************************************
 * calculates the observables of the host grid 
 * *********************************************** */
{
 int i, j, k;
 double rho, prs, tmp, vr, vth, vphi, v, v_t, prs_i, prs_w, prs_tot, p_cr, met, ne, E, dE=0.05, unit;
 double v1, v2, v3;
 char str[100];

 Q[0] = 0.0;
 Q[1] = 0.0;

 i = ii[0];
 j = ii[1];
 k = ii[2];


 rho    = data->rh[i + j*nx + k*nx*ny]*unit_rho;                   // in m_p/cc
 if (data->pr != NULL){
     prs = data->pr[i + j*nx + k*nx*ny]*unit_rho*unit_v*unit_v*1.67e-14;	// in cgs
     tmp = (prs/rho)*(0.6/1.38e-16) ;
 }
 if (data->mt != NULL){
     met = data->mt[i + j*nx + k*nx*ny];
 }else{
     met = 0.2;
 }

 ne     = rho;					// in particles/cc

 #if FREEFREE == 1
/* ********************************************** 
 * For emission in x-ray pure bremstrahlung emission
 * ********************************************* */
 unit = kpc/(4.0*3.14);
 Q[0] = unit*ne*ne*Lambda_freefree(tmp, 0.5,2.0);
 Q[1] = unit*ne*ne*Lambda_freefree(tmp, 2.0,10.0);
 return;
 #endif

 #if MEKAL_NOSPECTRA
 unit = kpc/(4.0*3.14);
 double *lam;
 lam = calloc(2, sizeof(double));
 XrayLambda(tmp, met, lam);

 Q[0] =  unit*ne*ne/(mu_i*mu_e)*lam[0];
 Q[1] =  unit*ne*ne/(mu_i*mu_e)*lam[1];

 free(lam);
 return;
 #endif

 #if COLDEN 	/* For column density */
 unit = kpc;
 Q[0] = rho*unit;

 if (rho != rho) { 
     print1("Density became NaN!\n", 0); 
     exit(1);
 }
 #endif


 #if EM == 1	/* For emission measure */
 unit = 3.08e21;
 if (tmp >= 2.4e6 && tmp <= 4.0e6){
     Q[0] = ne*ne*unit;			// cm^{-6}
 }else if (tmp >= 5.e6 && tmp <= 1e8){
     Q[1] = ne*ne*unit;			// cm^{-6}
 }else{
     Q[0] = 0.0;
     Q[1] = 0.0;
 }
 #endif

 #if OTH
 /* Put your emissivity here*/


 #endif

 return;
}


/* ********************************************************************* */
void FindOutboxQOfHost (double x, double y, double z, double *Q)
/* ***********************************************
 * calculates the observables of the host grid 
 * *********************************************** */
{
 int i, j, R;
 double rho, tmp, vr, vth, vphi, v, v_t, prs_i, prs_w, prs_tot, p_cr, met, ne, E, dE=0.05, unit;
 char str[100];

 Q[0] = 0.0;
 Q[1] = 0.0;

 R      = sqrt(x*x+y*y);
 
 rho    = d_hot(R, z)*1.0;            // in m_p/cc
 vr	= 0.0;
 vth	= 0.0;
 vphi	= 0.0;
 prs_i  = rho*T_HALO*1.38e-16/(mu);
 met    = 0.1;
 ne     = rho;                        // in electrons/cc
 tmp    = T_HALO;                        // in K

 #if FREEFREE
/* ********************************************** 
 * For emission in x-ray pure bremstrahlung emission
 * ********************************************* */
 unit = kpc/(4.0*3.14);
 Q[0] = unit*ne*ne*Lambda_freefree(tmp, 0.5,2.0);
 Q[1] = unit*ne*ne*Lambda_freefree(tmp, 2.0,10.0);
 return;
 #endif

 #if MEKAL_NOSPECTRA
 unit = 3.08e21/(4.0*3.14);
 double *lam;
 lam = calloc(2, sizeof(double));
 XrayLambda(tmp, met, lam);
 Q[0] =  unit*ne*ne/(mu_i*mu_e)*lam[0];
 Q[1] =  unit*ne*ne/(mu_i*mu_e)*lam[1];
 free(lam);
 #endif

 #if EM == 1	/* For emission measure */
 unit = 3.08e21;
 if (tmp >= 2.4e6 && tmp <= 4.0e6){
     Q[0] = ne*ne*unit;			// cm^{-6}
 }else if (tmp >= 5.e6 && tmp <= 1e8){
     Q[1] = ne*ne*unit;			// cm^{-6}
 }else{
     Q[0] = 0.0;
     Q[1] = 0.0;
 }
 #endif

 #if COLDEN == 1	/* For emission measure */
 unit = kpc;
 Q[0] = ne*unit;			// cm^{-2}
 #endif


 /* *******************************
  * Put your emissivity here      
  * ******************************* */
 #if OTH

 #endif

 return;
}



double LocalRhoOfHost (int *ii, Data *data)
/* ***********************************************
 * calculates the EM of the host grid 
 * *********************************************** */
{
 int i, j, k;
 double rho, prs, tmp, v1, v2, v3, v, v_t, prs_i, prs_w, prs_tot, p_cr, met, ne, Q, E, dE=0.05, unit;

 i = ii[0];
 j = ii[1];
 k = ii[2];

 rho    = data->rh[i + j*nx + k*nx*ny]*unit_rho;                   // in m_p/cc

 if (data->pr != NULL){
     prs = data->pr[i + j*nx + k*nx*ny]*unit_rho*unit_v*unit_v*1.67e-14;
     tmp = (prs/rho)*(mu/1.38e-16) ;
 }
 if (data->mt != NULL){
     met = data->mt[i + j*nx + k*nx*ny];
 }else{
     met = 0.2;
 }

 ne     = rho;                                       // in particles/cc
 unit = 3.08e21;
 if (v_t >= 20.0 && tmp > 1.e6)
     Q = rho*unit;
 else
     Q = 1.e-34;

 return Q;

}

/* ****************************************************************** */
double OutboxRhoOfHost (double x, double y, double z)
{
 double R;
 R = sqrt(x*x+y*y);
 
 return d_hot(R,z);
}


void FindLocalSpectraOfHost (int *ii, double **Q, Data *data)
/* ***********************************************
 * Calculates the spectra (0.1-4.0 keV) of the host grid. 
 * By default it has been set for mekal model.
 * VARIABLES:
 * lh [I]	= X,Y indices of the local host
 * Q [I/O]	= spectra of the host
 * *********************************************** */
{
 int i, j, k, l, s, imet, itmp;
 double rho, prs, tmp, ptmp, met, ne,  unit, Ebeg, Eend, met_pr, ptmp_pr;
 double unit_mekal, unit_ff, E;
 char str[50];

 i = ii[0];
 j = ii[1];
 k = ii[2];
  
 rho    = data->rh[i + j*nx + k*nx*ny]*unit_rho;                   // in m_p/cc
 if (data->pr != NULL)
     prs = data->pr[i + j*nx + k*nx*ny]*unit_rho*unit_v*unit_v*1.67e-14;
     tmp = (prs/rho)*(mu/1.38e-16) ;
 if (data->mt != NULL){
     met = data->mt[i + j*nx + k*nx*ny];
 }else{
     met = 0.2;
 }
 ne     = rho;                                          // in electrons/cc

// unit_mekal   = 3.08e21/(4.0*3.14)*(1.6e-9)*1.e-14;	//  is the grid separation, (..) \
                                                         is the energy value at that grid in erg \
                                                         1.e-14 is the normalisation constant from XPEC.\
							The unit is erg/s/cm^2/sr/keV \
							(not yet! it is still photons/s/cm^2/Sr/KeV times 1.6e-9\
							Final conversion to energy is done in integrate_los.c)
 unit_mekal	= 3.08e21/(4.0*3.14)*1.e-14;		// photons/s/cm^2/sr/keV

 unit_ff = 3.08e21/(4.0*3.14)*2.4e17*8.8e-38;	// erg/s/cm^2/sr/keV

 ptmp   = log10(tmp);			// getting the 'p'ower of temperature

 met_pr    = RoundMet(met); 		// rounding the met and ptmp to its closest existing value
 ptmp_pr   = RoundTmp(ptmp);
 imet   = ( int)(( met_pr -0.20)/0.20);	// converting the rounded met,ptmp to the indicies
 itmp   = ( int)(( ptmp_pr -6.0)/0.050)+1;

 for(k=0; k<nEbins; k++){      // defining the bins of the spectra from 0.1-3.0 keV
    Q[k][0]	= Ebeg + 0.002*k; 
    Q[k][1]	= 0.0; 
 }
  
 if (itmp < 0 || tmp < 4e5 ) return;

 if (tmp <= 3.e8){
     /* storing the output spectra in an 0.1-14.0 keV array. */
     for (s=0; s<nEbins; s++){
         Q[s][1]	= ne*ne/(mu_i*mu_e)*unit_mekal*dmekal[imet][itmp][s][1];
     }
 }else if (tmp <= 1e11){
     /* For free-free emission */
     for (s=0; s<nEbins; s++){
         E = Q[s][0];
         Q[s][1]	= ne*ne/(mu_i*mu_e)*unit_ff*pow(tmp, -0.5)*exp(-1.16*E*1.e7/tmp); 
     }
 }else{
     printf("! Warning: Temperature exceeds 10^11 K. The emission physics is unknown here ! \n");
//     exit(1);
 }

 }



/* ******************************************************* */
void FindOutboxSpectraOfHost(double x, double y, double z, double **Q)
/* *******************************************************
 * Getting the outbox (r > simulaion box size) spectra of host.
 * VARIABLES:
 * x,y,z [I]	= X,Y,Z positions of the host
 * Q [I/O]	= Spectra of the host grid.
 * ******************************************************* */

{
 int i, j, k, l, s, imet, itmp;
 char str1[200], str2[200];
 double R, rho, tmp, ptmp, met, ne, unit, Ebeg, Eend, met_pr, ptmp_pr;

 R	= sqrt(x*x+y*y);

 rho    = d_hot(R, z)*1.0;            // in m_p/cc
 met    = 0.2;
 ne     = rho;                                          // in electrons/cc
 tmp    = T_HALO;                        // in K

// unit   = 3.08e21/(4.0*3.14)*(1.6e-9)*1.e-14;
 unit	= 3.08e21/(4.0*3.14)*1.e-14;		// photons/s/cm^2/sr/keV
 ptmp   = log10(tmp);                   // getting the 'p'ower of temperature

 met_pr    = RoundMet(met);            // rounding the met and ptmp to its closest existing value
 ptmp_pr   = RoundTmp(ptmp);
 imet   = ( int)(( met_pr -0.20)/0.20); // converting the rounded met,ptmp to the indicies
 itmp   = ( int)(( ptmp_pr -6.0)/0.050)+1;

 for(k=0; k<nEbins; k++){      // defining the bins of the spectra from 0.1-3.0 keV
    Q[k][0]     = Ebeg + 0.002*k;
    Q[k][1]     = 0.0;
 }

 if (itmp < 0 || tmp < 4e5) return;

 for (s=0; s<nEbins; s++)                 // storing the output spectra (photons/s/keV/sr/cm^3) in a smaller (0.1-4.0 keV) array.
     Q[s][1]    = ne*ne/(mu_e*mu_i)*unit*dmekal[imet][itmp][s][1];

}


/* ****************************************
 * To get the density for r > rbox
 * *************************************** */
void FindOutboxEMOfHost(double x, double y, double z, double *Q) 
{
 double R, ne, unit = 3.08e21;
 R	= sqrt(x*x+y*y);
 ne	= d_hot(R, z)*1.0;

 Q[0] = 0.0;
 Q[1] = 0.0;

 if (T_HALO >= 2.4e6 & T_HALO <= 4.0e6){
     Q[0] = ne*ne*unit;			// cm^{-6} 
 }
 if (T_HALO >= 5e6 && T_HALO <= 1.e8){
     Q[1] = ne*ne*unit;
 }

 return;
}



/* *********************************** */
double FreeFree(double E, double T)
/* 
 E [I]	= energy in keV;
 T [I]  = temperature in K
*/
{
 return pow(T, -0.5)*exp(-1.16*E*1.e7/T);
}
