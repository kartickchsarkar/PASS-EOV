
#define ARRAY_1D(nx,type)          (type    *)Array1D(nx,sizeof(type))
#define ARRAY_2D(nx,ny,type)       (type   **)Array2D(nx,ny,sizeof(type))
#define ARRAY_3D(nx,ny,nz,type)    (type  ***)Array3D(nx,ny,nz,sizeof(type))
#define ARRAY_4D(nx,ny,nz,nv,type) (type ****)Array4D(nx,ny,nz,nv,sizeof(type))

/* *******************************************************
 * Function definitions 
 * ********************************************************/

typedef struct DATA {
  dtype *rh ;
  dtype *v1 ;
  dtype *v2 ;
  dtype *v3 ;
  dtype *pr ;
  dtype *mt ;
}Data;

int main(int, char **);
void WriteBegingingOfCode(int);
FILE *ChooseOutputName(char*, int);
void AllocateMemories(int, Data *);
void InitiateArrays(int);
void StoreGridFile(FILE *, int);
void StoreData(char **, int, Data *);
void SaveMekalModels(int);
void IntegrateAlongLOS(int, int, FILE *, int, Data *);
void FreeMemories(int, Data *);
void CombineOutput(FILE *, int);
void print1(char *, int );
void print_test(char *);

void FindLocalHost (double, double, double, int *);
void FindLocalQOfHost (int *, double *, Data *);
void FindOutboxQOfHost (double, double, double, double *);
double LocalRhoOfHost (int *, Data *);
double OutboxRhoOfHost (double, double, double);
void FindLocalSpectraOfHost (int *, double **, Data *) ;
void FindOutboxSpectraOfHost(double, double, double, double **);
double Lambda (double, double);
void XrayLambda (double, double, double *);
double Lambda_freefree (double, double, double);
char *Host(double, double, double);

dtype    *Array1D (int, size_t);
dtype   **Array2D (int, int, size_t);
dtype  ***Array3D (int, int, int, size_t);
dtype ****Array4D (int, int, int, int, size_t);
void FreeArray1D (void *);
void FreeArray2D (int, void **);
void FreeArray3D (int, int, void ***);
void FreeArray4D (int, int, int, void ****);
dtype **NullArray2D (dtype, dtype, int);

double RoundMet(double);
double RoundTmp(double);
// double Rend (double, double);
// double lmax (double, double);

double phi (double, double);
double d_hot (double, double);
double absolute (double);

void AnalyseData(char *);
void GetRunningFileNumber(char *, char *);

/* ************************************
 * Global variables 
 * ************************************ */
double **grid, *x1left, *x1right, *x2left, *x2right, *x3left, *x3right, ****dmekal;
double ***SBproc, ***SB;
int NARG;
long int g_usedMem;
time_t start_time;
time_t end_time;

#define nEbins	7000		// for energies till 7000*0.002=14 keV

