
#include "head.h"
void WriteSpecFile(double **, int, int);


void IntegrateAlongLOS(int m, int n, FILE *fout, int prank, Data *data)
/* 
 * Integrates quantities along a particular line of sight LOS.
 * The algorithm is as followed
 * i) start from solar poition which is at a distance of 8.5 kpc 
 *     from the Galactic centre. 
 * ii) move along the LOS by a differential length ds. 
 * iii) find the local host_stat i.e. the closest computational grid.
 * iv) calculate the required values/spectrum at that grid 
 * v) integrate the quantities/spectrum along that LOS. In case of
 *     Mekal model, we consider only 0.5-2.0 keV energies.
 * ----------------------------------------------------------
 * VARIABLE:
 * m [I]	= index for longitute
 * n [I] 	= index for latitude
 * fout [I/O]   = file handle in which the spectrum of a data is written.
 * ---------------------------------------------------------------------
 */
{

 double l, y0, z0, x_edge;
 double M, rho, *I, *Q;
 double xp, yp, zp;

 int k, s, kmax, *lh;
 double **spec, **intspec;
 char specfile[50], *host_stat, str1[200], str2[100];
 
 lh     	= calloc(3, sizeof(int));
 spec   	= ARRAY_2D(nEbins, 2, double);
 Q 		= calloc(2,sizeof(double));
 I 		= calloc(2,sizeof(double));

 y0 = m*dy0;
 z0 = n*dz0;
 fprintf (fout, "%2.4f %2.4f ", y0, z0);

 Q[0]	= Q[1]   = 0.0;
 I[0]	= I[1]   = 0.0;
 
 #if MEKAL || APEC || RS
 intspec	= ARRAY_2D(nEbins, 2, double);
 for (s = 0; s < nEbins; s++){		/* initiate the integrated spectrum to zero */
     intspec[s][0]  = 0.1 + 0.002*s;
     intspec[s][1] = 0.0;
 }
 #endif

 #if CONSIDER_EXT_BOX
 x_edge = sqrt(rout*rout-z0*z0-y0*y0);
 #else
   #if CARTESIAN
   x_edge = sqrt(xbox*xbox+zbox*zbox)*cos(atan(zbox/xbox)-THETA);
   #elif CYLINDRICAL
   x_edge = sqrt(Rbox*Rbox+zbox*zbox)*cos(atan(zbox/Rbox)-THETA);
   #elif SPHERICAL
   x_edge = rbox;
   #endif
 #endif

 l = 0.0;;
 while ( l < 2.0*x_edge){
     xp = -(x_edge-l)*cos(THETA) - z0*sin(THETA);
     yp = y0;
     zp = -(x_edge-l)*sin(THETA) + z0*cos(THETA);


     host_stat   = Host(xp, yp, zp);				// decides the host status , i.e. inside or outside etc.
     #if CONSIDER_EXT_BOX == 0					// A caution to ensure that the counting starts from the outer edge of box
     if (strcmp(host_stat, "InsideSimBox") != 0 ){
	 l += ds;
         continue; 
     }
     #endif
    

     #if MEKAL || APEC || RS				/* only for models that should generate spectra */
     if ( strcmp(host_stat,"InsideSimBox") == 0){ 			// if inside the computation box
         FindLocalHost (xp, yp, zp, lh);
         FindLocalSpectraOfHost (lh, spec, data);
     }else if (strcmp(host_stat,"InsideExtBox") == 0){			// if outside the computation box
         FindOutboxSpectraOfHost(xp, yp, zp, spec);
     }else{
         break;
     }
     #else
     if ( strcmp(host_stat,"InsideSimBox")==0){ 
         FindLocalHost (xp, yp, zp, lh);
         FindLocalQOfHost (lh, Q, data);
//         print_test(str);    
     }else if ( strcmp(host_stat,"InsideExtBox")==0){
         FindOutboxQOfHost(xp, yp, zp, Q);
     }else{
         break;
     }
     #endif

    /*  integrating along the LOS  */
     #if MEKAL || APEC || RS
     for (s = 0; s < nEbins; s++)       
         intspec[s][1]       += ds*spec[s][1];    // adding the photons or counts along LOS
     #endif

     I[0]       = I[0] + ds*Q[0];
     I[1]       = I[1] + ds*Q[1];

     l	+= ds;   
 }      // end the walk along LOS


/* Gathering the spectra in different energy bins */
 #if MEKAL || APEC || RS
 I[0] = I[1] = 0.0;
 for (s=49; s<=237; s++)                                  	// from E=0.5 keV to E = 2.0 keV
     I[0] +=  intspec[s][1]*0.002;     				// N(E)
//     I[0] +=  intspec[s][1]*intspec[s][0]*0.002;     		// N(E)*E*dE : Switch this on if cgs output is requires
 for (s=238; s< nEbins; s++)                      	   	//for 2.0-14.1 keV 
     I[1] +=  intspec[s][1]*0.002;
//     I[1] +=  intspec[s][1]*intspec[s][0]*0.002;		// switch this on if cgs output is required.
 #endif
 fprintf (fout, "%3.4e	%3.4e\n", I[0], I[1]); 


 int jj, kk;

 kk = EXISTS_NEG_Y ? m+g_mtotal/2 : m;
 jj = EXISTS_NEG_Z ? n+g_ntotal/2 : n;

 SBproc[kk][jj][0]	= I[0];					// Storing the data for later use (if any)
 SBproc[kk][jj][1]	= I[1];

 #if (MEKAL || APEC || RS) && WRITESPECTRA
 WriteSpecFile(intspec, m, n);
 #endif

 free(lh); 
 free(Q); free(I);
 FreeArray2D(nEbins, (void **)spec);
 #if MEKAL || APEC || RS
 FreeArray2D(nEbins, (void **)intspec);
 #endif 

}


/* ************************************************** */
void WriteSpecFile(double **intspec, int m, int n)
/*
 * Writes the 0.5-2.0 keV spectrum in mekal/l%db%d.mdl files in binary format.
 * First 2000*8 bytes are for energies and last 2000*8 bytes are the 
 * corresponding energy/flux/counts
 */

{
 int s, offset;
 double *E, *flux;
 char specfile[50];
 FILE *fspec;
//  nEbins = 2000; 	// number of bins in the energy spectrum // DO NOT CHANGE

 E 	= calloc(nEbins, sizeof(double));
 flux 	= calloc(nEbins, sizeof(double));
 for (s=0; s<nEbins; s++){
     E[s]	= intspec[s][0];
     flux[s]	= intspec[s][1];
 }

// sprintf(specfile, "mekal/l%db%d.mdl", ( int)(y*10.0), ( int)(y*10.0) );
 sprintf(specfile, "mekal/x%d_y%d.mdl", m, n );
 fspec = fopen(specfile, "wb");
 
 offset = 0;
 fseek(fspec, 0, SEEK_SET);
 fwrite(E, sizeof(double), nEbins, fspec);

 offset = sizeof(double)*(nEbins);
 fseek(fspec, offset, SEEK_SET);
 fwrite(flux, sizeof(double), nEbins, fspec);

/* fspec = fopen(specfile, "w");
 for (s=0; s<nEbins; s++)
     fprintf(fspec, "%12.6e %12.6e\n", E[s], flux[s]);
*/
 static int first_time_writting_spectra = 1;
 if (first_time_writting_spectra){
     FILE *freadme = fopen("mekal/README_MekalSpectraUser.txt","w");
     fprintf(freadme,"\n\n This binary files contain the spectral information along different line of sights .\n");
     fprintf(freadme,"\n First (%d x double) bits specifies energy.\n", nEbins);
     fprintf(freadme,"\n Second (%d x double) bits specifies corresponding flux (erg/s/cm^2/Sr/keV) or (photons/s/cm^2/Sr/keV) at that energy.\n", nEbins);
     fprintf(freadme,"\n\n\n\n You can plot the spectra in gnuplot with command  \n");
     fprintf(freadme,"\t\t p \"filename.mdl\" binary array=%d format=\"%%datatype\" skip=(%d*8*1) dx=0.002 w l\n\n", nEbins, nEbins);
     fprintf(freadme,"The Xrange of the energy extends from 0.1 to 4.0 keV in steps of 0.002 keV.\n");

     first_time_writting_spectra = 0;
/*     for (s=0; s<nEbins; s++){
        fprintf(freadme, "%2.4e\n", flux[s]); 
     } */
     fclose(freadme);
 }

 fclose(fspec);
 free(E); free(flux);
}
