
/* **************************************************************** 
 * Description of the gravitaional potential mainly to obtain the 
 * gas distribution outside the computational box. 
 * Required only if CONSIDER_EXT_BOX is set to YES 
 * **************************************************************** */

#define a       (3.0*kpc)
#define bp      (0.4*kpc)
#define c       12.0
#define a1      (6.0*kpc)
#define fc      (log(1.0+c)-c/(1.0+c))
#define ab      (2.0*kpc)

#define Mvir    (1.2e12*Msun)
#define rvir    (250.0*kpc)
#define rs      (rvir/c)
#define Mdisc   (6.e10*Msun)
#define Mb      (2.e10*Msun)

#define T_HALO  2.0e6           // galaxy model parameters
#define Tc      4.e4
#define Tcmz    1.e3
#define f       0.975

#define mu      0.6             // some density parameters
#define mu_e	1.15
#define mu_i	1.25
#define d0_hot  5.e-3           // in units of m_p/cc
#define d0_cold 1.e0
#define d0_cmz  0.0
#define csh     (sqrt(kB*T_HALO/(mu*mp)))
#define csc     (sqrt(kB*Tc/(mu*mp)))
#define cscmz   (sqrt(kB*Tcmz/(mp)))

double phi (double, double);
double dphidR (double, double);
double d_cold (double, double);
double d_cmz (double, double);
double d_hot (double, double);
double phi_nfw (double, double);
double phi_mn (double, double);
double phi_b (double, double);
double dphi_nfwdR (double);
double dphi_mndR (double);
double dphi_bdR (double);
double v_rot (double);
double absolute (double);

