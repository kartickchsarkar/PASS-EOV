
#include "head.h"

/* *************************************************************************
 * for rounding up the metallicity and the temperature inputs according to the need 
 * Met is rounded up in 0.2, 0.4, 0.6, 0.8, 1.0 levels
 * Temp is rounded up in .... 7.00, 7.05, 7.10, 7.15, ... levels
 * ***************************************************************** */
double RoundMet(double met)
{
 int k, divident, dividor, norm = 1000;
 double rmet, midlimit, remainder;

 dividor	= ( int)(0.2*norm);
 midlimit	= 0.2*norm/2.0;

 divident	= ( int)(met*norm);
 k		= divident/dividor;
 remainder	= divident%dividor;

 if (remainder > midlimit){
      rmet	= ( double)((k+1)*dividor);
 }else{
      rmet	= ( double)(k*dividor);
 }
 
 if (met <= 0.2){
     return 0.2;
 }else{
     return rmet/norm;
 }

}

/* ******************************************** */
double RoundTmp(double tmp)
{
 int k, divident, dividor, norm = 100000;
 double rtmp, midlimit, remainder;

 dividor        = ( int)(0.05*norm);
 midlimit       = 0.05*norm/2.0;

 divident       = ( int)(tmp*norm);
 k              = divident/dividor;
 remainder      = divident%dividor;

 if (remainder > midlimit){
      rtmp      = ( double)((k+1)*dividor);
 }else{
      rtmp      = ( double)(k*dividor);
 }

 return rtmp/norm;
}


/* ********************************************** */
FILE *ChooseOutputName(char *infile, int prank)
{
 char str[50], filetype[20];
 FILE *fp;
 char num[10], sbfile[50];

#if AUTOMATIC_FILENAME
  #if APEC
   sprintf(filetype,"%s","xr_apec");
  #elif RS
   sprintf(filetype,"%s","xr_rs");
  #elif FREEFREE
   sprintf(filetype,"%s","xr_ff");
  #elif MEKAL
   sprintf(filetype,"%s","xr_mkl");
  #elif MEKAL_NOSPECTRA
   sprintf(filetype,"%s","xr_mknospec");
  #elif COLDEN
   sprintf(filetype,"%s","colden");
  #elif EM
   sprintf(filetype,"%s","em");
  #elif OTH
   sprintf(filetype,"%s","oth");
  #else
   sprintf(filetype,"%s","undef");
  #endif

   GetRunningFileNumber(num, infile);
   if (filetype == "undef"){
       fp   = NULL ;
   }else{
       sprintf(sbfile,"output/%s.%s.sb",filetype, num);
       fp = fopen(sbfile,"w");
   }
   sprintf(str,"> Output filename choosen: automatically.");
#else
 fp = fopen(g_outfile,"w");
 sprintf(str,"> Output filename choosen: manually.");
#endif

 if (fp == NULL){
     print1("! Error:  Output filename not choosen. Returning now.", 0);
     exit(6);
 }else{
     print1(str, prank);
 }

 return fp;
}

/* ****************************************** */
void AllocateMemories(int prank, Data *data)
{
 int i, s;

 data->rh = calloc(nx*ny*nz, sizeof(dtype));
 #if MEKAL || MEKAL_NOSPECTRA || APEC || RS || FREEFREE || OTH
 data->pr = calloc(nx*ny*nz, sizeof(dtype));
 #else
 data->pr = NULL;
 #endif
 #if idx_met
 data->mt = calloc(nx*ny*nz, sizeof(dtype));
 #else
 data->mt = NULL;
 #endif
 data->v1 = NULL;
 data->v2 = NULL;
 data->v3 = NULL;

 #if MEKAL || APEC || RS
 dmekal = ARRAY_4D(5, 51, 9000, 2, double);
 #else
 dmekal = NULL;
 #endif

 SBproc	= ARRAY_3D(g_mtotal, g_ntotal, 2, double);
 SB	= ARRAY_3D(g_mtotal, g_ntotal, 2, double);


 x1left  = calloc (nx, sizeof(double));
 x1right = calloc (nx, sizeof(double));
 x2left  = calloc (ny, sizeof(double));
 x2right = calloc (ny, sizeof(double));
 x3left  = calloc (nz, sizeof(double));
 x3right = calloc (nz, sizeof(double));

 print1("> Memory allocated to the pointers.\n", prank);

 return;
}

/* ******************************* */
 void FreeMemories(int prank, Data *data)
{
 free((void *)data->rh); free((void *)data->v1); free((void *)data->v2); 
 free((void *)data->v3); free((void *)data->pr); free((void *)data->mt);

 free((void *)x1left); free((void *)x1right); 
 free((void *)x2left); free((void *)x2right); 
 free((void *)x3left); free((void *)x3right); 

 #if MEKAL || APEC || RS
 FreeArray4D(5, 51, 9000, (void ****)dmekal); 
 #endif
 
 char msg3[50]; 
 print1("\n\n> All memories are freed.\n\n", prank);
}


/* ********************************************* */
void StoreGridFile(FILE *gridfile, int prank)
{
 int i;
 double gleft, gright;
 double x1unit, x2unit, x3unit;

 #if SPHERICAL
   x1unit = unit_l;
   x2unit = 1.0;
   x3unit = 1.0;
 #elif CYLINDRICAL
   x1unit = unit_l;
   x2unit = unit_l;
   x3unit = 1.0;
 #elif CARTESIAN
   x1unit = unit_l;
   x2unit = unit_l;
   x3unit = unit_l;
 #endif 

 for (i = 0; i < nx+ny+nz; i++){
    if (gridfile == NULL){
	 printf("! Error: grid file pointer not found.\n");
	 exit(1);
    }
    fscanf(gridfile, "%lf %lf", &gleft, &gright);
    if (i < nx){
        x1left[i]       = gleft*x1unit;
        x1right[i]      = gright*x1unit;
    }else if (i < nx+ny){
        x2left[i-nx]    = gleft*x2unit;
        x2right[i-nx]   = gright*x2unit;
    }else if (i < nx+ny+nz){
        x3left[i-nx-ny]    = gleft*x3unit;
        x3right[i-nx-ny]   = gright*x3unit;
    }else{
        printf("! error : Could not read \"grid1.out\" properly. \n");
        exit(2);
    }
 }

 print1("> Grid data stored.\n",prank);

// printf("x1left[255]=%2.4e\n", x1left[255]);
/*
 char str[50];
 if (prank == 0){
     for (i=0; i<nx; i++){
         sprintf(str,"%2.4e \n", x1left[i]); 
         print_test(str);
     }
 }*/

 fclose(gridfile);
}

/* ********************************************************* */
void StoreData(char *files[], int prank, Data *data)
{
 int i, bt;
 FILE *fbin[NARG-1];
 for(i=0; i<NARG-1; i++) fbin[i] = fopen(files[i+1], "rb");

 #if single_file
   fseek (fbin[0], 0, SEEK_SET);
   fread (data->rh, nx*ny*nz, sizeof(dtype), fbin[0]);
   #if MEKAL || MEKAL_NOSPECTRA || APEC || RS || FREEFREE || OTH
   fseek (fbin[0], 0, SEEK_SET);
   for (i=0; i<idx_prs; i++) 
	for (bt=0; bt<8;  bt++) fseek(fbin[0], nx*ny*nz, SEEK_CUR);   // skipping nx*ny*nz*sizeof(double)*idx_prs bytes to read pressure
   fread (data->pr, nx*ny*nz, sizeof(dtype), fbin[0]); 
   #endif
   #if idx_met
   fseek (fbin[0], 0, SEEK_SET);
   for (i=0; i<idx_met; i++) 
	for (bt=0; bt<8;  bt++) fseek(fbin[0], nx*ny*nz, SEEK_CUR);   // skipping nx*ny*nz*sizeof(double)*idx_met bytes to read metalllicity
   fread (data->mt, nx*ny*nz, sizeof(dtype), fbin[0]); 
   #endif
 #else 
   fseek (fbin[0], 0, SEEK_SET);
   fread(data->rh, nx*ny*nz, sizeof(dtype), fbin[0]);
   #if MEKAL || MEKAL_NOSPECTRA || APEC || RS || FREEFREE || OTH
   fseek (fbin[1], 0, SEEK_SET);
   fread(data->pr, nx*ny*nz, sizeof(dtype), fbin[1]);
   #endif
   #if idx_met
   fseek(fbin[2], 0, SEEK_SET);
   fread(data->mt, nx*ny*nz, sizeof(dtype), fbin[2]);
   #endif

 #endif

 for(i=0; i<NARG-1; i++) fclose(fbin[i]);

 print1("> Simulation data stored in *d.\n",prank);
}


/* ******************************************************* */
void  SaveMekalModels(int prank)
{
 int imet, itmp, icount;
 float met, ptmp;
 double x1, x2;
 char spectrafile[100], model[10];
 FILE *fspectra;

 #if MEKAL
   sprintf(model,"mekal");
 #elif APEC
   sprintf(model,"apec");
 #elif RS
   sprintf(model,"rs");
 #else
   print1("!Warning: Spectral model has not been mentioned!",0);
 #endif

 met  = 0.2;
 while (met <= 1.1){
     ptmp       = 6.0;
     while (ptmp <= 8.5001){
         sprintf(spectrafile, "%s/%s%02d_%d.mdl", SpectraModelFolder, model, ( int)(met*10.0), ( int)(ptmp*100.0) );
         fspectra = fopen ( spectrafile, "r");
	 if (fspectra == NULL) print1("!Error: spectra model folder not found.\n",0);

         imet   = ( int)((met-0.2)/0.2);
         itmp   = ( int)((ptmp-6.0)/0.05);
         icount = 0;
         while(fscanf(fspectra, "%lf %lf\n", &x1, &x2) != EOF){
             dmekal[imet][itmp][icount][0] = x1;
             dmekal[imet][itmp][icount][1] = x2;
             icount++ ;
        }
//	printf("dmekal[0] = %2.4e,  dmekal[1]=%2.4e\n", dmekal[imet][itmp][10][0], dmekal[imet][itmp][10][1]);
     fclose(fspectra);
     ptmp       = ptmp + 0.05 ;
     }
 met    += 0.2;
 }
 
 print1("> spectral data is stored in ****dmekal.\n", prank);  

}




/* ***************************************** */
void WriteBegingingOfCode(int prank)
{
 char vers[100];

 print1("\n\n\n", prank);
/*
 print1("   ___  ___         ______   ______",prank);
 print1("  / _ \\/ _ \\       /___  /  /___  /", prank);
 print1(" / ___/ ___ \\  ___    / ___    /", prank);
 print1("/ /  / /   \\ \\/_____ / /_____ /", prank);
 print1("===================================\t", prank);  
 sprintf(vers,"Version: %2.2f", VERSION);
 print1(vers,prank);
 print1("\n\n", prank);
*/

 print1(" ____   _    ____ ____ ",prank);
 print1("|  _ \\ / \\  / ___/ ___| ",prank);
 print1("| |_) / _ \\ \\___ \\___ \\ ",prank);
 print1("|  __/ ___ \\ ___) |__) |",prank);
 print1("|_| /_/   \\_\\____/____/ ",prank);
 print1("===================================\t", prank);
 sprintf(vers,"Version: FB-%2.2f", VERSION);
 print1(vers,prank);
 print1("\n\n", prank);
 return;
}


/* ********************************************** */
void CombineOutput(FILE *fout, int nprocs)
{
 int i, count = 0, stat;
 double x1, x2, x3, x4;
 double cputime;
 char file[50], str[100];
 FILE *ftmp;

 end_time = time(NULL);
 cputime = (double)((end_time-start_time)/60.0);

 for (i=0; i<nprocs; i++){
     sprintf(file, "output/.outfile_%d.tmp", i);
     ftmp = fopen(file, "r");

     while( fscanf(ftmp,"%lf %lf %lf %lf", &x1, &x2, &x3, &x4) != EOF ){
	  if (count%g_mtotal == 0) fprintf(fout,"\n");
	  fprintf(fout, "%2.4f\t%2.4f\t%2.4e\t%2.4e\n", x1, x2, x3, x4);
	  count ++;
     }
     sprintf(str,"> Data from %s is stitched.\n", file);
     print1(str, 0);
     fclose(ftmp);
     stat = remove(file);
 }
 sprintf(str,"\n\n> Computation completed successfully.\n\n> Time taken: \t %3.2f minutes.\n\n\n", cputime);
 print1(str, 0);
 fclose(fout);

}


/* ************************************************ */
void print1(char *str, int prank)
{

 static int first_time = 1;

 if (prank == 0){
     if (g_logFile){
	 FILE *flog;
	 if (first_time){
	    flog = fopen("pass.log","w");
	    first_time = 0;
	 }else{
	    flog = fopen("pass.log","a");
	 }
         fprintf(flog, "%s \n", str);
	 fclose(flog);
     }else{
         printf("%s \n", str);
     }
 }

}

/* ************************************************ */
void print_test(char *str)
/* 
 * Prints test data to a test file */
{
 static int first_time2 = 1; 
 FILE *fp;

 if (first_time2){
    fp = fopen("test.log","w");
    first_time2 = 0;
 }else{
    fp = fopen("test.log","a");
 }

 fprintf(fp, "%s", str);
 fclose(fp); 

}

/* ****************************************** */
void GetRunningFileNumber(char num[], char *infile)
{
 char str[50], str2[10];
 memset(str2,'\0',sizeof(str2));
 sprintf(str, "%s", infile);
 #if single_file
   strncpy(str2, str+8, 4);
 #else
   strncpy(str2, str+7, 4);
 #endif

 sprintf(num,"%s",str2);
 return;
}

/* *************************************** */
void InitiateArrays(int prank)
{
 int i, j;

 for (j=0; j<g_mtotal; j++){
     for (i=0; i<g_ntotal; i++){
         SB[j][i][0]		= 0.0;
         SB[j][i][1]		= 0.0;
         SBproc[j][i][0]	= 0.0;
         SBproc[j][i][1]	= 0.0;
     }
 }
 print1("Arrays have been initialised to zero.",prank); 
}
