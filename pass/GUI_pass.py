#!/usr/bin/python
import numpy as np
from Tkinter import *
from tkMessageBox import *
import os
import sys

# Written by Kartick C Sarkar, Date: 09.06.2016
# email: kartick.c.sarkar100@gmail.com

class App:

    def __init__(self, master):
        frame = Frame(master)
        frame.grid(ipadx=40,ipady=30) 
#	frame.configure(bg="gray")	
#	frame.configure(font="Times 14 bold")	

	try:
	    os.stat("output")
	except:
	    os.mkdir("output")

	try:
	    os.stat("mekal")
	except:
	    os.mkdir("mekal")


        flag =StringVar()
	rout = StringVar()
	Label(frame,text=" Extended box ?", font="Times 14 bold").grid(row=0, sticky=W)
	R1 = Radiobutton(frame, text="Yes", variable=flag, value="YES").grid(row=0, column=1)
	R2 = Radiobutton(frame, text="No", variable=flag, value="NO").grid(row=1,column=1)
	self.EXTBOX = flag
	
#	self.lb2 = Label(frame, text=" Dimension").grid(row=3, column=0)
	Label(frame, text="Size of extended box ").grid(row=3, sticky=W)
	self.ROUT = Entry(frame, width=8)
	self.ROUT.grid(row=3, column=1)
	self.ROUT.insert(0,"100.0")
	Label(frame, text="[kpc]").grid(row=3, column=2)

	sf = StringVar()
	Label(frame, text="Single file?").grid(row=4, sticky=W)
	R3 = Radiobutton(frame, text="Yes", variable=sf, value="YES").grid(row=4, column=1)
	R4 = Radiobutton(frame, text="No", variable=sf, value="NO").grid(row=4, column=2)
	self.SF = sf

	Label(frame, text="Data type (double/float)").grid(row=5, sticky=W)
	self.DTYPE = Entry(frame, width=10)
	self.DTYPE.grid(row=5, column=1)
	self.DTYPE.insert(0,"float")

	emtype = StringVar()
	Label(frame, text="Emission type ", font="Times 14 bold").grid(row=6, sticky=W)
	ETR1 =  Radiobutton(frame, text="X-ray (e-p Bremstrahlung)", variable=emtype, value="ff").grid(row=6, column=1, sticky=W)
	ETR2 =  Radiobutton(frame, text="X-ray (Mekal)", variable=emtype, value="mekal").grid(row=7, column=1, sticky=W)
	ETR3 =  Radiobutton(frame, text="X-ray (Mekal- no spectra)", variable=emtype, value="mekal_nosp").grid(row=8, column=1, sticky=W)
	ETR4 =  Radiobutton(frame, text="X-ray (apec)", variable=emtype, value="apec").grid(row=9, column=1, sticky=W)
	ETR5 =  Radiobutton(frame, text="X-ray (rs)", variable=emtype, value="rs").grid(row=10, column=1, sticky=W)
#	ETR6 =  Radiobutton(frame, text="Synchrotron (23 GHz)", variable=emtype, value="sync23g").grid(row=11, column=1, sticky=W)
	ETR7 =  Radiobutton(frame, text="Column density", variable=emtype, value="colden").grid(row=12, column=1, sticky=W)
	ETR8 =  Radiobutton(frame, text="Emission Meausre", variable=emtype, value="em").grid(row=13, column=1, sticky=W)
	ETR9 =  Radiobutton(frame, text="Other", variable=emtype, value="oth").grid(row=14, column=1, sticky=W)	
	self.EMTYPE = emtype

        Label(frame, text="Projected surface specificaton:", font="Times 14 bold").grid(row=15, sticky=W)
        Label(frame, text="Number of y-grids").grid(row=16, sticky=W)
        self.PNY = Entry(frame, width=8)
        self.PNY.grid(row=16, column=1)
        self.PNY.insert(0,"256")

        Label(frame, text="Number of z-grids").grid(row=17, sticky=W)
        self.PNZ = Entry(frame, width=8)
        self.PNZ.grid(row=17, column=1)
        self.PNZ.insert(0,"256")

        Label(frame, text="Step size to walk along LOS").grid(row=18, sticky=W)
        self.PDS = Entry(frame, width=8)
        self.PDS.grid(row=18, column=1)
        self.PDS.insert(0,"0.1")
        Label(frame, text="[kpc]").grid(row=18, column=2)

        Label(frame, text="Viewing angle (wrt X-axis)").grid(row=19, sticky=W)
        self.THETA = Entry(frame, width=15)
        self.THETA.grid(row=19, column=1)
        self.THETA.insert(0,"1.57")


        Label(frame, text="Output specification:", font="Times 14 bold").grid(row=21, sticky=W)

	flag2 = StringVar()
        Label(frame, text="Write log file ?").grid(row=22, sticky=W)
	LGR3 = Radiobutton(frame, text="Yes", variable=flag2, value="YES").grid(row=22, column=1)
	LGR4 = Radiobutton(frame, text="No", variable=flag2, value="NO").grid(row=22,column=2)
	self.LOGFILE = flag2
	
	flag3 = StringVar()
        Label(frame, text="Write Spectra ?").grid(row=23, sticky=W)
	LGR5 = Radiobutton(frame, text="Yes", variable=flag3, value="YES").grid(row=23, column=1)
	LGR6 = Radiobutton(frame, text="No", variable=flag3, value="NO").grid(row=23,column=2)
	self.WSPECTRA = flag3

	flag4 = StringVar()
        Label(frame, text="Automatic filename ?").grid(row=24, sticky=W)
	LGR7 = Radiobutton(frame, text="Yes", variable=flag4, value="YES").grid(row=24, column=1)
	LGR8 = Radiobutton(frame, text="No", variable=flag4, value="NO").grid(row=24,column=2)
	self.AUTOFILE = flag4

	flag5 = StringVar()
        Label(frame, text="Run in Parallel ?").grid(row=25, sticky=W)
	LGR9 = Radiobutton(frame, text="Yes", variable=flag5, value="YES").grid(row=25, column=1)
	LGR10 = Radiobutton(frame, text="No", variable=flag5, value="NO").grid(row=25,column=2)
	self.PARALLEL = flag5

        Label(frame, text="Output file name").grid(row=26, sticky=W)
        self.OUTFILE = Entry(frame, width=20)
        self.OUTFILE.grid(row=26, column=1)
        self.OUTFILE.insert(0,"output/user.txt")
#	if ( flag4 == "NO"):
#	    self.OUTFILE.config(state='disabled')

        Label(frame, text="Pimms folder address").grid(row=27, sticky=W)
        self.PIMMS = Entry(frame, width=20)
        self.PIMMS.grid(row=27, column=1)
        self.PIMMS.insert(0,"/home/kcsarkar/softwares/pimms/models/")



    def GetSimulationParameters(self):
	geom	= "SPHERICAL"
	dim	= "1"
	comp	= "1"
	nvar	= "6"
	x1	= "1.0"
	x2	= "1.0"
	x3	= "1.0"
	nx	= "1"
	ny	= "1"
	nz	= "1"
	dt	= "1.0"
	irho	= "0"
	ivx1	= "0"
	ivx2	= "0"
	ivx3	= "0"
	iprs	= "0"
	itr1	= "0"
	itmp	= "0"

	f = open("../grid.out","r")
	txt = f.readlines()
	grd = []
	for line in txt:
            a = line.split()
	    if (len(a) == 1):
		continue
            if (a[0] == "#"):
          	if (a[1] == "DIMENSIONS:"):
            	    dim = a[2]
            	    continue
        	if (a[1] == "GEOMETRY:"):
                    geom = a[2]
                    continue
        	if (a[1] == "X1:"):
		    tmpr = a[2].split('[')
		    tmpr2 = tmpr[1].split(',')
		    x1beg = tmpr2[0]
                    tmpr = a[3].split(']')
            	    x1end  = tmpr[0]
            	    nx = a[4]
            	    continue
        	if (a[1] == "X2:"):
		    tmpr = a[2].split('[')
		    tmpr2 = tmpr[1].split(',')
		    x2beg = tmpr2[0]
                    tmpr = a[3].split(']')
            	    x2end  = tmpr[0]
            	    ny = a[4]
            	    continue
        	if (a[1] == "X3:"):
		    tmpr = a[2].split('[')
		    tmpr2 = tmpr[1].split(',')
		    x3beg = tmpr2[0]
                    tmpr = a[3].split(']')
            	    x3end  = tmpr[0]
            	    nz = a[4]
            	    continue
    	    else:
                grd.append(a[1:3])
    	f.close()

	fg = open("grid1.out","w")
	for i in range(0,len(grd)):
    	    fg.write(grd[i][0]+"\t"+grd[i][1]+"\n")
	fg.close()

	fdbl = open("../dbl.out","r")
	lines = fdbl.readlines()
	line0 = lines[0].split()
	if (len(lines)==1):
	    dt = 1.0
	else:
	    line1 = lines[1].split()
	    dt = float(line1[1])- float(line0[1])
	nvar = int(len(line0))-6
	for i in range(0,len(line0)):
    	    if (line0[i] == "rho"):
            	irho = i-6
    	    if (line0[i] == "vx1"):
        	ivx1 = i-6
    	    if (line0[i] == "vx2"):
        	ivx2 = i-6
    	    if (line0[i] == "vx3"):
        	ivx3 = i-6
    	    if (line0[i] == "prs"):
        	iprs = i-6
    	    if (line0[i] == "tr1"):
        	itr1 = i-6
    	    if (line0[i] == "tr2"):
        	itr2 = i-6
    	    if (line0[i] == "T"):
        	iT = i-6
	fdbl.close()

	fpl = open("../pluto.log","r")
	txt = fpl.readlines()
 	for line in txt:
 	    a = line.split()
 	    if (len(a) <= 1):
 	        continue
	    if (a[0] == "step:0"):
		break
 	    if (a[0] == "DIMENSIONS:"):
 	        dim2 = a[1]
 	    if (a[0] == "COMPONENTS:"):
 	        comp = a[1]
 	    if (a[0] == "TRACERS:"):
 	        ntr = int(a[1])
 	    if (a[0] == "VARIABLES:"):
 	        nvarp = int(a[1])
 	    if (a[0] == "[Density]:"):
 	        urho = str(float(a[1])/1.67e-24)
 	    if (a[0] == "[Velocity]:"):
 	        uvx = str(float(a[1])/1.e5)
 	    if (a[0] == "[Length]:"):
 	        ulen = str(float(a[1])/3.08e21)

	# check dimensions 
	tnvar = nvarp + ntr + 1
#	if (nvar != tnvar):
#    	    print "Variable number mismatch !"
#            exit()
 	if (dim != dim2):
    	    print "Dimension number mismatch !"
    	    exit()
	fpl.close()

	ulen_flt = float(ulen)
	if (geom == "SPHERICAL"):
	    x1unit	= ulen_flt
	    x2unit	= 1.0
	    x3unit	= 1.0
	elif (geom == "CYLINDRICAL"):
	    x1unit	= ulen_flt
	    x2unit	= ulen_flt
	    x3unit	= 1.0
	else:
	    x1unit	= ulen_flt
	    x2unit	= ulen_flt
	    x3unit	= ulen_flt
	    
	    
	self.GM		= geom
	self.DIM	= dim
	self.COMP	= comp
	self.NVAR	= str(nvar)
	self.X1BEG	= str(float(x1beg)*x1unit)
	self.X1END	= str(float(x1end)*x1unit)
	self.X2BEG	= str(float(x2beg)*x2unit)
	self.X2END	= str(float(x2end)*x2unit)
	self.X3BEG	= str(float(x3beg)*x3unit)
	self.X3END	= str(float(x3end)*x3unit)
	self.NX		= nx
	self.NY		= ny
	self.NZ		= nz
	self.DT		= dt
	self.IRHO	= str(irho)
	self.IVX1	= str(ivx1)
	self.IVX2	= str(ivx2)
	self.IVX3	= str(ivx3)
	self.IPRS	= str(iprs)
	self.ITR1	= str(itr1)
#	self.ITMP	= str(iT)
	self.ULNG	= ulen;
	self.URHO	= urho
	self.UVX	= uvx

	self.NEGY = "NO"
	self.NEGZ = "NO"
	if (geom == "CARTESIAN"):
	    if (float(x2beg)<0.0):
	        self.NEGY = "YES"
	    if (float(x3beg)<0.0):
		self.NEGZ = "YES"
	if (geom == "CYLINDRICAL"):
	    if (float(x3beg)<0.0):
		self.NEGZ = "YES"
	    if (dim == "3"):
		self.NEGY = "YES"
	if (geom == "SPHERICAL"):
	    if(float(x2end)> 3.15/2.):
		self.NEGZ = "YES"
	    if (dim == "3"):
		self.NEGY = "YES"

    def create_header(self):
	self.GetSimulationParameters()
#	print self.EXTBOX.get()
	if (self.EXTBOX.get() == ""):
	    askretrycancel(title="Error !", message="Shall I consider an extended box or not ?")
	    return True
	if (self.ROUT.get() == "" and self.EXTBOX.get() == "YES"):
	    askretrycancel(title="Error !", message="Extended box size has not been mentioned !")
	    return True
	if (self.EMTYPE.get() == ""):
	    askretrycancel(title="Error !", message="Please be specific about the emission type you want !")
	    return True
	if (self.LOGFILE.get() == ""):
	    askretrycancel(title="Error !", message="Please mention if I need to write a run-time log file")
	    return True
	if (self.WSPECTRA.get() == ""):
	    askretrycancel(title="Error !", message="You are not specific whether to write spectra or not !")
	    return True
	if (self.AUTOFILE.get() == ""):
	    askretrycancel(title="Error !", message="Do I choose output filename by myself or you will provide me ? Please be specific")
	    return True
	if (self.OUTFILE.get() == "" and self.AUTOFILE.get() == "YES"):
	    askretrycancel(title="Error !", message="User defined output filename has not been mentioned !")
	    return True

	f = open("head.h","w")
	f.write("\n\n")
	f.write("#ifndef HEAD_H\n")
	f.write("#define HEAD_H\n\n")
	f.write("#include<stdlib.h>\n")
	f.write("#include<stdio.h>\n")
	f.write("#include<math.h>\n")
	f.write("#include<string.h>\n")
	f.write("#include<time.h>\n")
	f.write("#include<sys/types.h>\n")
	f.write("#include<sys/stat.h>\n")
	f.write("#include<unistd.h>\n\n")

	f.write("/* Define macros */\n")
	f.write("#define VERSION 1.2\n\n")
	f.write("#define YES     1\n#define NO      0\n\n")

	f.write("#define PARALLEL\t"+self.PARALLEL.get()+"\n\n")
	f.write("#if PARALLEL\n")
	f.write("  #include<mpi.h>\n")
	f.write("#endif\n\n")


	f.write("#define CONST_G 6.67e-8\n")
	f.write("#define kB      1.38e-16\n")
	f.write("#define CONST_PI 3.14159265359\n")
	f.write("#define pc      3.08e18\n")
	f.write("#define kpc     (1.e3*pc)\n")
	f.write("#define Msun    2.e33\n")
	f.write("#define mp	 1.67e-24\n")
	f.write("#define km      1.e5\n")
	f.write("#define yr      3.15e7\n")
	f.write("#define Myr     (1.e6*yr)\n\n")
	f.write("#define DIMENSIONS\t"+self.DIM+"\n")
	f.write("#define COMPONENTS\t"+self.COMP+"\n\n")
	f.write("#if DIMENSIONS == 1\n")
	f.write("    #define EXPAND(a,b,c)       a\n")
	f.write("#elif DIMENSIONS == 2\n")
	f.write("    #define EXPAND(a,b,c)       a b\n")
	f.write("#elif DIMENSIONS == 3 || COMPONENTS == 3\n")
	f.write("    #define EXPAND(a,b,c)       a b c\n")
	f.write("#endif\n\n")
	f.write("/* ***************************************************************************\n")
	f.write(" * Galactic model definitions\n")
	f.write(" * => required only in case extended box is considered otherwise do not modify\n")
	f.write(" * *************************************************************************** */\n\n")
	f.write("#include \"grav.h\"\n\n")
	f.write("/* ***************************************************\n")
	f.write(" * Simulation details\n")
	f.write(" ***************************************************** */\n")

	if (self.GM == "CARTESIAN"):
	    f.write("#define CARTESIAN\tYES\n")
	    f.write("#define SPHERICAL\tNO\n")
	    f.write("#define CYLINDRICAL\tNO\n")
	if (self.GM == "SPHERICAL"):
	    f.write("#define CARTESIAN\tNO\n")
	    f.write("#define SPHERICAL\tYES\n")
	    f.write("#define CYLINDRICAL\tNO\n")
	if (self.GM == "CYLINDRICAL"):
	    f.write("#define CARTESIAN\tNO\n")
	    f.write("#define SPHERICAL\tNO\n")
	    f.write("#define CYLINDRICAL\tYES\n")

	# Any parameter that is set using calculations inside the python can be accessed as self.AB
	# But, any parameter that is read from the GUI is only accessible by self.AB.get()

	f.write("#define CONSIDER_EXT_BOX	" + self.EXTBOX.get() + "\n\n")

	f.write("#if CARTESIAN\n")
	f.write("  #define xbox\t" + self.X1END + "\n")
	f.write("  #define ybox\t" + self.X2END + "\n")
	f.write("  #define zbox\t" + self.X3END + "\n")
	f.write("#elif SPHERICAL\n")
	f.write("  #define rbox\t" + self.X1END + "\n")
	f.write("#elif CYLINDRICAL\n")
	f.write("  #define Rbox\t" + self.X1END +"\n")
	f.write("  #define zbox\t" + self.X2END +"\n")
	f.write("#endif\n\n")

	f.write("#define EXISTS_NEG_Y\t" + self.NEGY +"\n")
	f.write("#define EXISTS_NEG_Z\t" + self.NEGZ + "\n\n")

	f.write("#if CONSIDER_EXT_BOX\n")
	f.write("  #define rout\t" + self.ROUT.get() + "\n")
	f.write("#endif\n\n")

	f.write("#define nx\t" + self.NX + "\n")
	f.write("#define ny\t" + self.NY + "\n")
	f.write("#define nz\t" + self.NZ + "\n")
	f.write("#define nvar\t" + self.NVAR + "\n")
	f.write("#define dtype\t" + self.DTYPE.get() + "\n\n")

	f.write("#define idx_rho\t" + self.IRHO + "\n")
	f.write("#define idx_vx1\t" + self.IVX1 + "\n")
	f.write("#define idx_vx2\t" + self.IVX2 + "\n")
	f.write("#define idx_vx3\t" + self.IVX3 + "\n")
	f.write("#define idx_prs\t" + self.IPRS + "\n")
	f.write("#define idx_met\t" + self.ITR1 + "\n")
#	f.write("#define idx_temp\t" + self.ITMP + "\n\n")
	f.write("#define unit_l\t" + self.ULNG + "\t\t//in kpc\n")
	f.write("#define unit_rho\t" + self.URHO + "\t\t//in m_p/cc\n")
	f.write("#define unit_v\t" + self.UVX + "\t\t// km/s\n\n")
	f.write("#define single_file\t" + self.SF.get() + "\n\n")

	FF		= "NO"
	MEKAL		= "NO"
	MEKAL_NOSP	= "NO"
	APEC		= "NO"
	RS		= "NO"
#	SYNC23G		= "NO"
	COLDEN		= "NO"
	EM		= "NO"
	OTH		= "NO"
	EMTYPE = self.EMTYPE.get()
#	print EMTYPE
	if (EMTYPE == "ff"):
	    FF	= "YES"
	if (EMTYPE == "mekal"):	
	    MEKAL	= "YES"
	if (EMTYPE == "mekal_nosp"):
	    MEKAL_NOSP	= "YES"
	if (EMTYPE == "apec"):	
	    APEC	= "YES"
	if (EMTYPE == "rs"):
	    RS	= "YES"
#	if (EMTYPE == "sync23g"):	
#	    SYNC23G	= "YES"
	if (EMTYPE == "colden"):
	    COLDEN	= "YES"
	if (EMTYPE == "em"):	
	    EM	= "YES"
	if (EMTYPE == "oth"):	
	    OTH	= "YES"

	f.write("/* **********************************************\n")
	f.write(" * output emission type specifications\n")
	f.write(" ************************************************ */\n")
	f.write("#define FREEFREE\t" + FF +"\n")
	f.write("#define MEKAL\t" + MEKAL +"\n")
	f.write("#define MEKAL_NOSPECTRA\t"+MEKAL_NOSP+"\n")
	f.write("#define APEC\t"+ APEC +"\n")
	f.write("#define RS\t"+ RS +"\n")
#	f.write("#define SYNC23G\t"+SYNC23G+"\n")
	f.write("#define COLDEN\t"+COLDEN+"\n")
	f.write("#define EM\t"+EM+"\n")
	f.write("#define OTH\t"+OTH+"\n\n\n\n")
	f.write("/* **********************************************\n")
	f.write(" * Projected surface specification\n")
	f.write(" ************************************************ */\n")
	f.write("#define g_mtotal\t"+self.PNY.get()+"\n")
	f.write("#define g_ntotal\t"+self.PNZ.get()+"\n")
	f.write("#define ds\t"+self.PDS.get()+"\n")
	f.write("#define THETA\t"+self.THETA.get()+"\n\n")
	

	f.write("#if CARTESIAN\n")
	f.write("  #if EXISTS_NEG_Y\n")
	f.write("    #define dy0	 	 (2.0*ybox/(g_mtotal*1.0))	// from -ybox to +ybox\n")
	f.write("  #else\n")
	f.write("    #define dy0                 (ybox/(g_mtotal*1.0))\n")
        f.write("  #endif\n")
        f.write("  #if EXISTS_NEG_Z\n")
        f.write("      #define dz0               (2.0*zbox/(g_ntotal*1.0))\n")
	f.write("  #else\n")
        f.write("      #define dz0               (zbox/(g_ntotal*1.0))\n")
        f.write("  #endif\n\n")
        f.write("#elif SPHERICAL\n")
	f.write("  #if EXISTS_NEG_Y\n")
        f.write("    #define dy0                 (2.0*rbox/(g_mtotal*1.0))\n")
	f.write("  #else\n")
        f.write("    #define dy0                 (rbox/(g_mtotal*1.0))\n")
	f.write("  #endif\n")
        f.write("  #if EXISTS_NEG_Z\n")
        f.write("    #define dz0                 (2.0*rbox/(g_ntotal*1.0))\n")
        f.write("  #else\n")
	f.write("    #define dz0                 (rbox/(g_ntotal*1.0))\n")
        f.write("  #endif\n\n")
        f.write("#elif CYLINDRICAL\n")
	f.write("  #if EXISTS_NEG_Y\n")
        f.write("    #define dy0                 (2.0*Rbox/(g_mtotal*1.0))\n")
	f.write("  #else\n")
        f.write("    #define dy0                 (Rbox/(g_mtotal*1.0))\n")
	f.write("  #endif\n")
        f.write("  #if EXISTS_NEG_Z\n")
        f.write("      #define dz0               (2.0*zbox/(g_ntotal*1.0))\n")
        f.write("  #else\n")
        f.write("      #define dz0               (zbox/(g_ntotal*1.0))\n")
	f.write("  #endif\n")
	f.write("#endif\n\n")


	f.write("/* *****************************************************\n")
	f.write(" * Output filename specifications\n")
	f.write(" * ***************************************************** */\n")
	f.write("#define g_logFile\t"+self.LOGFILE.get()+"\n")
	f.write("#define WRITESPECTRA\t"+self.WSPECTRA.get()+"\n")
	f.write("#define AUTOMATIC_FILENAME\t"+self.AUTOFILE.get()+"\n\n")
	f.write("static char *SpectraModelFolder\t= \""+ self.PIMMS.get() +"\";\n")
	f.write("static char *GridFile           = \"grid1.out\";\n")
	f.write("static char *g_outfile          = \""+ self.OUTFILE.get() +"\";\n\n\n\n")
	f.write("#include \"globals.h\"\n\n\n\n")
	f.write("#endif\n")
	f.write("\n")
	f.close()
        showinfo(title=None, message="Header file has been created successfully")
        	

root=Tk()
app=App(root)
root.title("PASS")

menubar = Menu(root)
savemenu = Menu(menubar,tearoff=0)
menubar.add_command(label='Create Header File', command=app.create_header)
menubar.add_command(label='Quit',command=root.quit)
root.config(menu=menubar)

# showinfo(title=None, message="Header file has been created successfully", **options)
#root.grid(ipadx=200,ipady=300)

#self.lb1 = Label(frame,text=" Geometry").grid(row=0, column=0)

#def create_header(self):
#    self.lb2 = Label(root,text="Test is running").grid(row=1, column=0)
root.mainloop() 

