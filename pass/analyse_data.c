#include "head.h"

void AnalyseData(char *infile)
/* Function to analyse the surface brightness data if required */ 
{

}

/* ******************************************** 
 * Example to analyse data
 * ********************************************

void AnalyseData(char *infile)
{
 char num1[50], num2[10], numb[10], outfile[50];
 GetRunningFileNumber(numb, infile);
 sprintf(outfile,"output/xr.%s.ana",numb);

 FILE *fana = fopen(outfile,"w");
 fprintf(fana, "# This is analysed from %s.\n", infile);

 int m, n; 
 double z_planer = 0.0;
 double dy, dz, *y, *z, *sbavg_soft, *sbavg_hard, tot_sb, totsb2, totsb3, totsb4;
 y = calloc(g_mtotal, sizeof(double));
 z = calloc(g_ntotal, sizeof(double));
 sbavg_soft = calloc(g_ntotal, sizeof(double));
 sbavg_hard = calloc(g_ntotal, sizeof(double));

 #if SPHERICAL
 dy = rbox/(g_mtotal*1.0);
 dz = rbox/(g_ntotal*1.0);
 #elif CYLINDRICAL || CARTESIAN
 dy = Rbox/(g_mtotal*1.0);
 dz = zbox/(g_ntotal*1.0);
 #endif

 tot_sb = totsb2 = totsb3 = totsb4 = 0.0; 
 for (n=0; n<g_ntotal; n++){	// horizontally averaged vertical surface brightness profile 
     z[n] = n*dz;
     sbavg_soft[n] = sbavg_hard[n] = 0.0;
     for (m=0; m<g_mtotal-1; m++){
	 sbavg_soft[n] += (SB[m][n][0]+SB[m+1][n][0])*dy/(2.0*g_mtotal*dy);	// unit of erg/s/cm^2/Sr
	 sbavg_hard[n] += (SB[m][n][1]+SB[m+1][n][1])*dy/(2.0*g_mtotal*dy);
     }
     if (z[n] >= z_planer) tot_sb += (sbavg_soft[n-1]+sbavg_soft[n])*(g_mtotal*dy)*dz/2.0;
     if (z[n] >= 0.2) totsb2 += (sbavg_soft[n-1]+sbavg_soft[n])*(g_mtotal*dy)*dz/2.0;
     if (z[n] >= 0.5) totsb3 += (sbavg_soft[n-1]+sbavg_soft[n])*(g_mtotal*dy)*dz/2.0;
     if (z[n] >= 1.0) totsb4 += (sbavg_soft[n-1]+sbavg_soft[n])*(g_mtotal*dy)*dz/2.0;
 }
 tot_sb *= 4.0*kpc*kpc;		// Since the surgace brightness is actually calculated in one quadrant.\
				Notice, the only positive values of y and z are considered in m and n
 totsb2	*= 4.0*kpc*kpc;
 totsb3	*= 4.0*kpc*kpc;
 totsb4	*= 4.0*kpc*kpc;
 
 fprintf(fana, "# Averaged surface brightness profile (erg/s/cm^2/Sr) along vertical direction.\n");
 fprintf(fana, "# z [kpc]	suface brightness.\n");
 fprintf(fana, "# 		soft(0.5-2.0)	hard(2.0-4.0)\n\n\n");
 for (n=0; n < g_ntotal; n++)
     fprintf(fana, "%2.4e	%2.4e	%2.4e \n", z[n], sbavg_soft[n], sbavg_hard[n]);
         

//  Following is to write the time variation of total extraplaner emission
 int nf;
 double t, dt;
 FILE *fr, *fr2, *fr3, *fr4;
 dt = 1.0;
 nf = atoi(numb);
 t = nf*dt;
 if ( strcmp(numb,"0000") == 0){
     fr = fopen("output/tvslx_zgt0.txt","w");
     fprintf(fr,"# This is Extra planer (|z| > %2.2f kpc) emission in units of erg/s/Sr .\n", z_planer);
     fprintf(fr,"# t [Myr]	Ix [erg/s/Sr] \n\n");
     fr2 = fopen("output/tvslx_zgtp2.txt","w");
     fprintf(fr2,"# This is Extra planer (|z| > %2.2f kpc) emission in units of erg/s/Sr .\n", 0.2);
     fprintf(fr2,"# t [Myr]	Ix [erg/s/Sr] \n\n");
     fr3 = fopen("output/tvslx_zgtp5.txt","w");
     fprintf(fr3,"# This is Extra planer (|z| > %2.2f kpc) emission in units of erg/s/Sr .\n", 0.5);
     fprintf(fr3,"# t [Myr]	Ix [erg/s/Sr] \n\n");
     fr4 = fopen("output/tvslx_zgt1.txt","w");
     fprintf(fr4,"# This is Extra planer (|z| > %2.2f kpc) emission in units of erg/s/Sr .\n", 1.0);
     fprintf(fr4,"# t [Myr]	Ix [erg/s/Sr] \n\n");
 }else{
     fr = fopen("output/tvslx_zgt0.txt","a");
     fr2 = fopen("output/tvslx_zgtp2.txt","a");
     fr3 = fopen("output/tvslx_zgtp5.txt","a");
     fr4 = fopen("output/tvslx_zgt1.txt","a");
 }
 fprintf(fr,"%2.2f	%2.4e \n", t, tot_sb);
 fprintf(fr2,"%2.2f	%2.4e \n", t, totsb2);
 fprintf(fr3,"%2.2f	%2.4e \n", t, totsb3);
 fprintf(fr4,"%2.2f	%2.4e \n", t, totsb4);


 fclose(fana); fclose(fr); fclose(fr2); fclose(fr3); fclose(fr4);
 free(y); free(z); free(sbavg_soft); free(sbavg_hard);

 print1(">> Data analysed successfully.",0);
}

*/
