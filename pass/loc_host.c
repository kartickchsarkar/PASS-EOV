
#include "head.h"

void FindLocalHost (double x, double y, double z, int *ii)
/* ******************************************************************
 * Find the local host (closest simulation grid point) of the given 
 * position 
 * VARIABLES:
 * x,y,z [I]	= X, Y, Z cartesian positions (in kpc)
 * \return X,Y indices of the 2D data points 
 * ****************************************************************** */
{
 int i, j, k;
 double x1, x2, x3;
 
 #if CARTESIAN
 x1  = x;
 x2  = y;
 x3  = z;
 #elif SPHERICAL			// r, theta, phi
 x1  = sqrt(x*x +y*y + z*z);		
 x2  = acos(z/x1);
 x3  = atan(y/x);
 #elif CYLINDRICAL		// r, z, phi
 x1 = sqrt(x*x+y*y);
 x2 = z;
 x3 = atan(y/x);
 #endif 

 ii[0]  = 0;
 ii[1]  = 0;
 ii[2]  = 0;

 for (i=0; i< nx; i++){
     if (x1 >= x1left[i] && x1 < x1right[i]){
         ii[0] = i;
         break;
     }
 }

 for (j=0; j<ny; j++){
     if (x2 >= x2left[j] && x2 < x2right[j]){
         ii[1] = j;
         break;
     }
 }

 for (k=0; k<nz; k++){
     if (x3 >= x3left[k] && x3 < x3right[k]){
         ii[2] = k;
         break;
     }
 }

 return;

}



/* ************************************************* */
char *Host(double x, double y, double z)
/* 
 * Decides whether the host is inside the sim box or inside the extended simulation box
 * or completely outside the box under consideration.
 * VARIABLES:
 * x, y, z [I] 	= X, Y, Z coordinate of the host
 * \return a string
 * **************************************** */
{
 double rhost, Rhost, zhost;
 char *stat;

 rhost = sqrt(x*x+y*y+z*z);
 Rhost = sqrt(x*x+y*y);
 zhost = z;
 
 #if CARTESIAN
 if ((fabs(x) <= xbox) & (fabs(y) <= ybox) & (fabs(z) <= zbox)){
     stat = "InsideSimBox";
 #if CONSIDER_EXT_BOX
 }else if (rhost <= rout){
     stat = "InsideExtBox";
 #endif
 }else{
     stat = "OutsideExtBox";
 }
 #endif		// end of cartesian


 #if SPHERICAL
 if ( rhost <= rbox ){
     stat = "InsideSimBox";
 #if CONSIDER_EXT_BOX
 }else if (rhost <= rout){
     stat = "InsideExtBox";
 #endif
 }else{
     stat = "OutsideExtBox";
 }
 #endif		// end spherical

 #if CYLINDRICAL
 if (( Rhost <= Rbox ) && ( zhost <= zbox )){
     stat = "InsideSimBox";
 #if CONSIDER_EXT_BOX
 }else if (rhost <= rout){
     stat = "InsideExtBox";
 #endif
 }else{
     stat = "OutsideExtBox";
 }
 #endif		// end cylindrical



 return stat;
}

